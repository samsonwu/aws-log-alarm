## No longer login AWS!!!

``` shell
yarn ts "FacebookClient#ErrorResponse|Request failed with status code 400" 60 100
```

`First arg` - log keywords, can be separated by `|`

`Second arg` - log period (mins), default 30

`Third arg` - number of logs, default 30

## The query uses frequently

### Facebook Client

#### Search facebook client error

``` shell
yarn ts "FacebookClient#ErrorResponse" 60 100
```

#### Search facebook live comment hook fails

``` shell
yarn ts "FacebookFeedFail" 60 100
```

#### Search facebook status change hook fails

``` shell
yarn ts "FacebookLiveVideosFail" 60 100
```

### Number of messages sent ???

### Number of checkout ???

### Search by received keyword ???