/* eslint-disable no-console */
import { CloudWatchLogs } from 'aws-sdk';
import { appendFileSync } from 'fs';
import moment from 'moment';

import { queryGenerator } from './queries/messageLike';

const awsLogs = new CloudWatchLogs({
  region: 'ap-southeast-1',
});

const queryLogs = (queryString: string, mins: number) => {
  return awsLogs.startQuery({
    queryString,
    logGroupName: '/shopline/eks/general-eks',
    startTime: moment().add(-mins, 'minutes').valueOf(),
    endTime: moment().valueOf(),
    limit: 3
  }).promise();
};

const queryResult = async (queryId: string) => {
  const result = await awsLogs.getQueryResults({ queryId }).promise();
  if (result.status === 'Complete') {
    for (const val of result.results) {
      const popResult = `${val[0].value} --- ${JSON.stringify(JSON.parse(JSON.parse(val[1].value).log))} \n`;
      appendFileSync('aws.log', popResult);
      console.log(popResult);
    }
  } else {
    console.log(`The query is ${result.status}`);
    setTimeout(async () => await queryResult(queryId), 2000);
  }
};

(async () => {
  const args = process.argv.slice(2);
  const { queryId } = await queryLogs(queryGenerator((args[0].split('|')), Number(args[2] ?? 30)), Number(args[1] ?? 30));
  await queryResult(queryId);
})();