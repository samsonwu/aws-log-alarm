export const queryGenerator = (likeStates: string[], limit = 20) => {
  let query = `fields @timestamp, @message | sort @timestamp desc | limit ${limit}`;
  for (const likeState of likeStates) {
    query += ` | filter @message like /${likeState}/`;
  }
  return query;
};